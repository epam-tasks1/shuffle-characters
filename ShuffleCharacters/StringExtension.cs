﻿using System;

namespace ShuffleCharacters
{
    public static class StringExtension
    {
        public static string ShuffleChars(string source, int count)
        {
            if (string.IsNullOrEmpty(source) || string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentException("source is null or empty.");
            }

            if (count < 0)
            {
                throw new ArgumentException("count is less than zero.");
            }

            int arraySize = 0;
            if (source.Length % 2 != 0)
            {
                arraySize++;
            }

            char[] left = new char[source.Length / 2];
            char[] right = new char[(source.Length / 2) + arraySize];

            string result = source;
            int iterationCount = 0;

            do
            {
                result = ShuffleCharsMethod(result, left, right);
                iterationCount++;
            }
            while (source != result);

            if (count > iterationCount)
            {
                while (count > iterationCount)
                {
                    count -= iterationCount;
                }

                count += iterationCount;
            }

            for (int i = 0; i < count; i++)
            {
                result = ShuffleCharsMethod(result, left, right);
            }

            return result;
        }

        public static string ShuffleCharsMethod(string source, char[] left, char[] right)
        {
            if (source is null)
            {
                throw new ArgumentException("source is null.");
            }

            if (left is null)
            {
                throw new ArgumentException("left is null.");
            }

            if (right is null)
            {
                throw new ArgumentException("right is null.");
            }

            int currentIndex = 0;
            for (int j = 0; j < source.Length; j++)
            {
                if (j % 2 == 0)
                {
                    right[currentIndex] = source[j];
                    continue;
                }

                left[currentIndex] = source[j];
                currentIndex++;
            }

            return string.Concat(new string(right), new string(left));
        }
    }
}
